#include "box.hpp"
#include "common.hpp"

using namespace glm;

Box::message::message():
	id_newblock(-1),
	dir(none),
	is_move(false),
	pauze(false),
	is_tick(false),
	go_on(true)
{
}

Box::Box(int X, int Y, int Z, const Block *blocks, int blocks_count):
	m_blocks(blocks),
	m_blocks_count(blocks_count)
{
	m_dims[0] = X;
	m_dims[1] = Y;
	m_dims[2] = Z;
	
	m_cells = new Cell**[m_dims[1]];
	for(int y=0; y<m_dims[1]; y++)
	{
		m_cells[y] = new Cell*[m_dims[0]];
		for(int x=0; x<m_dims[0]; x++)
		{
			m_cells[y][x] = new Cell[m_dims[2]];
			for(int z=0; z<m_dims[2]; z++)
			{
				m_cells[y][x][z] = 0;
			}
		}
	}
}

Box::~Box()
{
	for(int y=0; y<m_dims[1]; y++)
	{
		for(int x=0; x<m_dims[0]; x++)
		{
			delete[] m_cells[y][x];
		}
		delete[] m_cells[y];
	}
	delete[] m_cells;
}
	
bool Box::NewCycle(message &msg)
{
	m_toberemoved.clear();
	m_currentcells.clear();

	int random_number;
	if(msg.id_newblock == -1)
	{
		random_number = RandomNumber(m_blocks_count);
		msg.id_newblock = random_number;
	}
	else
	{
		random_number = msg.id_newblock;
	}
	m_current = &m_blocks[random_number];
	m_currentpos = glm::vec3(m_dims[0]/2, m_dims[1]-1, m_dims[2]/2);
	m_transform = mat4(1.f);
	
	bool finished = RePosition();
	
	return finished;
}

bool Box::Insert(Cell *cell, const Block *inserted)
{
	bool ok = !(*cell);
	if(!ok)
	{
		/*
		 * the cell is allowed to be in cell
		 */
		for(std::deque<Cell*>::iterator it=m_currentcells.begin(); it != m_currentcells.end(); ++it)
		{
			if(cell == *it)
			{
				ok = true;
				break;
			}
		}
	}
	if(ok)
	{
		*cell = m_current;
		return true;
	}
	else
	{
		return false;
	}
}

bool Box::RePosition()
{
	int n = m_current->GetCubeCount();
	const glm::vec3 *cubes = m_current->GetCubes();
	
	bool finished=false;
	int inserted = 0;
	
	/*
	 * backup old cells, erase all old cells, empty currentcells
	 */
	std::deque<Cell*> oldcells(m_currentcells);
	for(int i=0; i<oldcells.size(); i++)
	{
		Cell *cell = oldcells[i];
		*cell = 0;
	}
	m_currentcells.clear();
	
	vec3 transformed[n];
	vec3 ref(100.f, 100.f, 100.f);
	for(int i=0; i<n; i++)
	{
		transformed[i] = vec3(m_transform * vec4(cubes[i], 1.f));
		
		if(transformed[i].x < ref.x)
		{
			ref.x = transformed[i].x;
		}
		if(transformed[i].y < ref.y)
		{
			ref.y = transformed[i].y;
		}
		if(transformed[i].z < ref.z)
		{
			ref.z = transformed[i].z;
		}
	}
	
	for(int i=0; i<n; i++)
	{
		/*
		 * insert cells
		 */
		vec3 pos = m_currentpos + transformed[i] - ref;

		bool fitXZ = pos.x >= 0 && pos.x < m_dims[0] && pos.z >= 0 && pos.z < m_dims[2];
		bool fitY = pos.y < m_dims[1];
		if(!fitXZ)
		{
			finished = true;
			break;
		}
		else if(fitY)
		{
			Cell *cell = GetCell(pos.x, pos.y, pos.z);
			finished = !Insert(cell, m_current);
			if(finished)
			{
				break;
			}
			m_currentcells.push_back(cell);
			++inserted;
		}
	}
	if(finished)
	{
		/*
		 * undo insert cells
		 */
		for(int i=0; i<inserted; i++)
		{
			Cell *cell = m_currentcells.back();
			m_currentcells.pop_back();
			
			*cell = 0;
		}
		/*
		 * restore currentcells
		 */
		m_currentcells = oldcells;
		for(int i=0; i<m_currentcells.size(); i++)
		{
			Cell *cell = m_currentcells[i];
			*cell = m_current;
		}
	}
	else
	{
		/**
		 * do nothing
		 */
	}
	
	return finished;
}

bool Box::Down(message &msg)
{
	m_mutex.lock();
	
	int Y = --m_currentpos.y;
	
	if(Y<-1)
	{
		if(Y==-2)
		{
			/*
			 * erase
			 */
			int erased = 0;
			for(std::list<int>::iterator it = m_toberemoved.begin(); it!=m_toberemoved.end(); ++it)
			{
				/*
				 * remove all cells at this height
				 */
				int Y = *it - erased;
				Echo(Y);
				Cell **level = m_cells[Y];
				
				for(int x=0; x<m_dims[0]; x++)
				{
					for(int z=0; z<m_dims[2]; z++)
					{
						Cell *cell = GetCell(x,Y,z);
						*cell = 0;
					}
				}
				Echo("erased");
				/*
				 * shuffle content down
				 */
				for(int y=Y; y<m_dims[1]-1; y++)
				{
					m_cells[y] = m_cells[y+1];
				}
				Echo("shuffled");
				/*
				 * replace upper level by erased one
				 */
				m_cells[m_dims[1]-1] = level;
				Echo("replaced");
				++erased;
			}
			
			/*
			 * there is no longer an active block that can be manipulated using Down
			 */
			
			m_current = 0;
						
			m_mutex.unlock();
			
			return false;
		}
		
		/*
		 * start new cycle
		 */
		 		
		m_mutex.unlock();
		
		return NewCycle(msg);
		
	}
	
	//Y = --m_currentpos.y;
	/*
	 * reposition
	 */

	bool finished = Y==-1 || RePosition();
	if(finished)
	{
		/*
		 * check first and last row , bottom to top
		 */
		for(int y= (Y < 0 ? 0 : Y); y<m_dims[1]; y++)
		{
			bool toberemoved = false;
			
			{
				//check rows parallel to z
				bool filled=false;
				
				for(int x=0; x<m_dims[0]; x++)
				{
					filled = false;
						
					for(int z=0; z<m_dims[2]; z++)
					{
						filled = *GetCell(x, y, z);
						if(filled)
						{
							break;
						}
					}
					if(!filled)
					{
						break;
					}
				}
				if(filled)
				{
					m_toberemoved.push_back(y);
					toberemoved = true;
				}
			}
			if(!toberemoved)
			{
				//check rows parallel to x
				bool filled=false;
				
				for(int z=0; z<m_dims[2]; z++)
				{
					filled=false;
						
					for(int x=0; x<m_dims[0]; x++)
					{
						filled = *GetCell(x, y, z);
						if(filled)
						{
							break;
						}
					}
					if(!filled)
					{
						break;
					}
				}
				if(filled)
				{
					m_toberemoved.push_back(y);
					toberemoved = true;
				}
			}
		}
		
		/*
		 * reduce list
		 */
		m_toberemoved.unique();
		/*
		 * no longer active block
		 */
		m_current = 0;
		/*
		 * mark as no longer moving
		 */
		m_currentpos.y = -1;
		
		if(m_toberemoved.size()>0)
		{
			/*
			 * wait for second tick to destroy
			 * now all lower rows to be destroyed are at the back of m_toberemoved
			 */
			
			m_mutex.unlock();
			 
			return false;
		}
		
		m_mutex.unlock();
		
		return NewCycle(msg);
	}

	m_mutex.unlock();
	
	return false;
}

Box::Cell *Box::GetCell(int x, int y, int z)
{
	return &m_cells[y][x][z];
}

void Box::Init(message &msg)
{
	NewCycle(msg);
}

Box::Cell ***Box::GetCells() const
{
	return m_cells;
}

void Box::Arrow(bool x, direction arrow)
{
	m_mutex.lock();
	
	if(m_current)
	{
	
		if(arrow == up)
		{
			mat4 rotation;
			
			rotation = x ? m_current->GetRotZ() : m_current->GetRotX();
			
			mat4 oldtransform = m_transform;
			m_transform = rotation * m_transform;
			
			bool finished = RePosition();
			if(finished)
			{
				m_transform = oldtransform;
			}
		}
		else
		{
			glm::vec3 pos = m_currentpos;
			if(arrow == down)
			{
				if(m_currentpos.y)
				{
					m_currentpos.y -= 1.f;
				}
				else
				{
					m_mutex.unlock();
					
					return;
				}
			}
			else if(x)
			{
				m_currentpos.x += (arrow == right) ? -1.f : 1.f;
			}
			else
			{
				m_currentpos.z += (arrow == left) ? -1.f : 1.f;
			}
			
			bool  hit = RePosition();
			if(hit)
			{
				m_currentpos = pos;
				RePosition();
			}
		}
	}
	
	m_mutex.unlock();
}
