#ifndef BLOCK_H
#define BLOCK_H

#include <glm/glm.hpp>


class Block
{
public:
	Block();
	~Block();

	void Build(const char *filename);
	
	glm::vec3 GetColor() const;
	const glm::vec3 *GetCubes() const;
	int GetCubeCount() const;
	const glm::mat4 &GetRotX() const, &GetRotZ() const;
	
private:
	glm::vec3 m_color;
	int m_cubes_count;
	glm::vec3 *m_cubes;
	
	glm::mat4 m_rotX, m_rotZ;
};

#endif
