#include "common.hpp"
#include <iostream>
#include <stdlib.h>

using namespace std;

void ThrowError(const char *e) throw (const char*)
{
	cerr << e << endl;
	throw e;
}

void Echo(const char *e, bool newline)
{
	/*cout << e;
	if(newline)
	{
		cout << endl;
	}*/
}

void Echo(float f, bool newline)
{
	/*cout << f;
	if(newline)
	{
		cout << endl;
	}*/
}

int RandomNumber(int range)
{
	return rand() % range;
}
