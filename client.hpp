#ifndef CLIENT_H
#define CLIENT_H

#include "message.hpp"
#include <boost/asio.hpp>
#include <boost/thread/thread.hpp>
#include <deque>

typedef std::deque<Message> msgdeque;
typedef void handler(const boost::system::error_code& error);

class Client
{
public:	
	enum  code {connection_lost, connection_made, connection_waiting};
	typedef void callbackfstate(code);
	typedef void callbackfmessage(const char*);
	
	static callbackfstate callbackfs0;
	static callbackfmessage callbackfm0;
	
public:
	Client(char *host, char *port, callbackfstate *cbfs=&callbackfs0, callbackfmessage *cbfm=&callbackfm0);
	~Client();
	
	void Write(const Message&);
	void Close();
	void Join();
	int GetID();
	void WaitForConnection();
	
private:
	handler handle_connected, handle_header_read, handle_body_read;
	void handle_written(const boost::system::error_code& error);
	void do_write(Message), do_close(), do_read();

private:
	boost::asio::io_service m_ioservice;
	boost::asio::ip::tcp::socket m_socket;
	Message m_readmsg;
	msgdeque m_writemsgs;
	const int m_port;
	callbackfstate *const m_callbackfstate;
	callbackfmessage *const m_callbackfmessage;
	int m_id;
	boost::thread m_thread;
	boost::condition_variable m_cond_connected;
	boost::mutex m_mutex_connection;
};

#endif
