#ifndef COMMON_H
#define COMMON_H

void ThrowError(const char*) throw (const char*);

void Echo(float, bool newline=true);
void Echo(const char*, bool newline=true);

int RandomNumber(int range);

#endif
