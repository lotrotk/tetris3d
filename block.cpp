#include "block.hpp"
#include "common.hpp"

#include <fstream>
#include <sstream>
#include <glm/gtc/matrix_transform.hpp>

using namespace glm;
using namespace std;

Block::Block():
	m_cubes_count(0),
	m_cubes(0)
{
}

Block::~Block()
{
	delete[] m_cubes;
}

void Block::Build(const char *filename)
{
	ifstream ifs;
	ifs.open(filename);
	
	if(ifs.fail())
	{
        ThrowError("file not found");
	}

	string s, t;
	{
		//color
		getline(ifs, s);
		stringstream ss(s);
		ss >> t >> m_color.x >> m_color.y >> m_color.z;
	}
	{
		//numcubes
		getline(ifs, s);
		stringstream ss(s);
		ss >> t >> m_cubes_count;
	}
	m_cubes = new vec3[m_cubes_count];
	int numcubesX=0;
	{
		//numcubesX
		getline(ifs, s);
		stringstream ss(s);
		ss >> t >> numcubesX;
	}
	vec3 *p=m_cubes;
	{
		//cubesX
		for(int i=0; i<numcubesX; i++)
		{
			getline(ifs, s);
			stringstream ss(s);
			ss >> t >> p->x >> p->y;
			p->z =0;
			++p;
		}
	}
	int numcubesZ=0;
	{
		//numcubesZ
		getline(ifs, s);
		stringstream ss(s);
		ss >> t >> numcubesZ;
	}
	if(numcubesX+numcubesZ != m_cubes_count)
	{
		ThrowError("number of cubes does not match");
	}
	{
		//cubesZ
		for(int i=0; i<numcubesZ; i++)
		{
			getline(ifs, s);
			stringstream ss(s);
			ss >> t >> p->z >> p->y;
			p->x =0;
			++p;
		}
	}
	
	ifs.close();
	
	{
		/*
		 * rotation matrices
		 */
		
		m_rotZ = translate(mat4(1.f), vec3(-0.5f, -0.5f, 0.f))
					*
					rotate(mat4(1.f), -90.f, vec3(0.f, 0.f, 1.f))
					*
					translate(mat4(1.f), vec3(0.5f, 0.5f, 0.f));
		
		m_rotX = translate(mat4(1.f), vec3(0.f, -0.5f, -0.5f))
					*
					rotate(mat4(1.f), 90.f, vec3(1.f, 0.f, 0.f))
					*
					translate(mat4(1.f), vec3(0.f, 0.5f, 0.5f));
					
		mat4 Block::*mats[2] = {&Block::m_rotX, &Block::m_rotZ};
		for(int i=0; i<2; i++)
		{
			for(int j=0; j<4; j++)
			{
				for(int k=0; k<4; k++)
				{
					(this->*mats[i])[j][k] = round((this->*mats[i])[j][k]);
				}
			}
		}
	}
}

glm::vec3 Block::GetColor() const
{
	return m_color;
}

const glm::vec3 *Block::GetCubes() const
{
	return m_cubes;
}

int Block::GetCubeCount() const
{
	return m_cubes_count;
}

const mat4 &Block::GetRotX() const
{
	return m_rotX;
}

const mat4 &Block::GetRotZ() const
{
	return m_rotZ;
}
