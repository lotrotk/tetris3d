#include "common.hpp"
#include "box.hpp"
#include "client.hpp"

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <boost/thread/thread.hpp>

using namespace glm;

namespace g
{
	const int width=350, height=700;
	const int X = 10, Y = 20, Z = X;
	const float screen_far = 30.0f, screen_near = 3.f;
	const int ms=1000;
	
	const int numblocks = 3;
	Block blocks[numblocks];
	const char *filenames[numblocks] = {"block0.block", "block1.block", "block2.block"};
	
	GLuint buffers[4];
	GLuint program;
	GLint attribute_coord3d, uniform_color, uniform_viewprojection, uniform_modeltoworld;
	
	Box *box=0;
	Client *client=0;
	bool i_am_x;
	
	float angle = 0.f;
	mat4 transformtoraster;	
	
	bool fromup = false;
	bool rotated = true;
	bool perspective = false;
	bool goon = true;
	bool drawraster = false;
	bool pauze = false;
	bool game_over = false;
}

void send(Box::message *msg);

void Tick()
{
	while(g::goon && !g::game_over)
	{
		if(!g::pauze)
		{
			Box::message msg;
			msg.is_tick = true;
			g::game_over = g::box->Down(msg);
			msg.game_over = g::game_over;
			if(g::client)
			{
				send(&msg);
			}
		}
		
		boost::this_thread::sleep(boost::posix_time::milliseconds(g::ms));
	}
	
	if(g::game_over)
	{
		Echo("Game over!");
	}
}

void mousemoved(GLFWwindow * const window, double x, double y)
{
	static double x0 = 0;
	static double t0 = glfwGetTime();

	const double t = glfwGetTime();
	const double speed = 0.5;

	if(glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS)
	{
		const double delta = (x - x0) * speed * (t - t0);
		
		if(delta == 0)
		{
			return;
		}
		
		g::angle -= delta*0.5f;
		
		g::rotated = true;
	}

	x0 = x;
	t0 = t;
}

void keycall(GLFWwindow * const window, int key, int scancode, int action, int mods)
{
	if(action == GLFW_PRESS)
	{
		bool x;
		Box::direction dir;
		
		switch(key)
		{
			case GLFW_KEY_UP:
				x = true;
				dir = Box::up;
				break;
			case GLFW_KEY_LEFT:
				x = true;
				dir = Box::left;
				break;
			case GLFW_KEY_RIGHT:
				x = true;
				dir = Box::right;
				break;
			case GLFW_KEY_DOWN:
				x = true;
				dir = Box::down;
				break;
			case 'S':
				x = false;
				dir = Box::down;
				break;
			case 'A':
				x = false;
				dir = Box::left;
				break;
			case 'D':
				x = false;
				dir = Box::right;
				break;
			case 'W':
				x = false;
				dir = Box::up;
				break;
			default:
				dir = Box::none;
		}
		
		if(dir != Box::none && !g::pauze && !g::game_over)
		{
			if(g::client)
			{
				x = g::i_am_x;
				Box::message msg;
				msg.dir = dir;
				msg.game_over = g::game_over;
				send(&msg);
			}
			g::box->Arrow(x, dir);
		}
		else
		{
			switch(key)
			{
				case GLFW_KEY_F1:
					g::angle = 0.f;
					g::rotated = true;
					break;
				case GLFW_KEY_F2:
					g::angle = 90.f;
					g::rotated = true;
					break;
				case GLFW_KEY_F3:
					g::perspective = !g::perspective;
					g::rotated = true;
					break;
				case GLFW_KEY_F4:
					g::fromup = !g::fromup;
					g::rotated = true;
					break;
				case 'A':
					g::drawraster = !g::drawraster;
					break;
				case GLFW_KEY_SPACE:
					g::pauze = !g::pauze;
					if(g::client)
					{
						Box::message msg;
						msg.pauze = g::pauze;
						send(&msg);
					}
					break;
			}
		}
	}
	else
	{
		if(key == GLFW_KEY_ESCAPE)
		{
			g::goon = false;
		}
	}
}

void view()
{
	if(g::rotated)
	{
		const vec3 center((g::X-1)*0.5f, (g::Y-1)*0.5f, (g::Z-1)*0.5f);
		const vec3 position = (g::fromup ? vec3((g::X-1)*0.5f, (g::Y-1)+0.5f, (g::Z-1)*0.5f) : vec3((g::X-1)*0.5f, (g::Y-1)*0.5f, 0.f));
		const vec3 delta = (position - center);
		
		mat4 rot = rotate(mat4(1.0f), g::angle, vec3(0.f, 1.f, 0.f));
		vec3 p = vec3(rot * vec4(delta , 1.f)) + center;
		mat4 view = lookAt(p, center, (g::fromup ? vec3(rot[2][0], 0.f, rot[0][0]) : vec3(0.f, 1.f, 0.f)));

		const mat4 *projection;

		if(!g::perspective)
		{
			/*
			 * orthogonal projection matrix
			 */
			const float u = 1.0f/(g::screen_far+g::screen_near);

			const mat4 proj = mat4(
				2.f/g::X, 0.f, 0.f, 0.f,
				0.f, 2.f/g::Y, 0.f, 0.f,
				0.f, 0.f, 2.f*u, 0.f,
				0.f, 0.f, (g::screen_far-g::screen_near)*u, 1.f);
				
			projection = &proj;
		}
		else
		{
			/*
			 * perspective projection matrix
			 */
			const float cot = 2.41421356f;//45° fov
			const float na = cot*g::Y*0.5f;
			const float near = na - g::screen_near;
			const float far = near + g::screen_far;
			const float ratio = 1.0f*g::X/g::Y;
			const float u = 1.f/(near - far);
			const mat4 proj = mat4(
				cot/ratio, 0.f, 0.f, 0.f,
				0.f, cot, 0.f, 0.f,
				0.f, 0.f, u*(far + near), -1.f,
				0.f, 0.f, u*far*near*2.f, 0.f)
				* translate(mat4(1.0f), vec3(0.f, 0.f, -na));;
				
			projection = &proj;
		}
                                
		view = (*projection)*view;
		
		glUseProgram(g::program);
		glUniformMatrix4fv(g::uniform_viewprojection, 1, GL_FALSE, value_ptr(view));
		
		g::rotated = false;
	}
}

void onDisplay()
{
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glUseProgram(g::program);
	
	/*
	 * blocks
	 */
	glEnableVertexAttribArray(g::attribute_coord3d);
	
	glBindBuffer(GL_ARRAY_BUFFER, g::buffers[0]);
	glVertexAttribPointer(g::attribute_coord3d, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, g::buffers[1]);
	
	Box::Cell ***cells = g::box->GetCells();
	mat4 world(1.0f);
	int drawn = 0;
	for(int y=0; y<g::Y; y++)
	{
		world[3][1] = y;
		for(int x=0; x<g::X; x++)
		{
			world[3][0] = x;
			for(int z=0; z<g::Z; z++)
			{
				const Block *block = cells[y][x][z];
				if(block)
				{
					vec3 color = block->GetColor();
					glUniform3f(g::uniform_color, color.x, color.y, color.z);
					
					world[3][2] = z;
										
					glUniformMatrix4fv(g::uniform_modeltoworld, 1, GL_FALSE, value_ptr(world));
					
					
					//glPolygonMode(GL_FRONT, GL_FILL);
					glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_SHORT, 0);
					
					/*
					 * outline
					 */
					glBindBuffer(GL_ARRAY_BUFFER, g::buffers[3]);
					glVertexAttribPointer(g::attribute_coord3d, 3, GL_FLOAT, GL_FALSE, 0, 0);
					glUniform3f(g::uniform_color, 0.9f, 0.9f, 1.f);
					
					glDrawArrays(GL_LINES, 0, 24);
					
					++drawn;
				}
			}
		}
	}
	
	/*
	 * raster
	 */
	GLuint buf;
	int ind_count;
	
	if(g::drawraster)
	{
		buf = g::buffers[2];
		const int raster_indices_count = ((g::X+1)*(g::Y+1)+(g::X+1)*(g::Z+1)+(g::Y+1)*(g::Z+1))*2;
		ind_count = raster_indices_count;
		world= mat4(1.f);
	}
	else
	{
		/*
		 * outline
		 */
		buf = g::buffers[3];
		ind_count = 24;
		world = g::transformtoraster;
	}
	
	glUniformMatrix4fv(g::uniform_modeltoworld, 1, GL_FALSE, value_ptr(world)); 
	
	glBindBuffer(GL_ARRAY_BUFFER, buf);
	glVertexAttribPointer(g::attribute_coord3d, 3, GL_FLOAT, GL_FALSE, 0, 0);
	
	glUniform3f(g::uniform_color, 0.9f, 0.9f, 1.f);
	
	
	glDrawArrays(GL_LINES, 0, ind_count);
	

	
	glDisableVertexAttribArray(g::attribute_coord3d);
}

void init()
{
	
	GLfloat vertices[]=
	{
		-0.5f, -0.5f, 0.5f,
		-0.5f, -0.5f, -0.5f,
		0.5f, -0.5f, 0.5f,
		0.5f, -0.5f, -0.5f,
		-0.5f, 0.5f, 0.5f,
		-0.5f, 0.5f, -0.5f,
		0.5f, 0.5f, 0.5f,
		0.5f, 0.5f, -0.5f,
	};
	GLushort indices[]=
	{
		0, 1, 2,
		2, 1, 3,
		0, 2, 4,
		4, 2, 6,
		4, 6, 5,
		5, 6, 7,
		3, 1, 7,
		7, 1, 5,
		1, 0, 5,
		5, 0, 4,
		2, 3, 6,
		6, 3, 7,
	};
	GLfloat raster[((g::X+1)*(g::Y+1)+(g::X+1)*(g::Z+1)+(g::Y+1)*(g::Z+1))*2*3];
	{
		GLfloat *ptr = raster;
		for(int x=0; x <= g::X; x++)
		{
			for(int y=0; y <= g::Y; y++)
			{
				ptr[0] = -0.5f + x; ptr[1] = -0.5f + y; ptr[2] = -0.5f;
				ptr[3] = -0.5f + x; ptr[4] = -0.5f + y; ptr[5] = 0.5f + g::Z;
				ptr += 6;
			}
		}
		for(int x=0; x <= g::X; x++)
		{
			for(int z=0; z <= g::Z; z++)
			{
				ptr[0] = -0.5f + x; ptr[1] = -0.5f; ptr[2] = -0.5f + z;
				ptr[3] = -0.5f + x; ptr[4] = 0.5f + g::Y; ptr[5] = -0.5f + z;
				ptr += 6;
			}
		}
		for(int y=0; y <= g::Y; y++)
		{
			for(int z=0; z <= g::Z; z++)
			{
				ptr[0] = -0.5f; ptr[1] = -0.5f + y; ptr[2] = -0.5f + z;
				ptr[3] = 0.5f + g::X; ptr[4] = -0.5f + y; ptr[5] = -0.5f + z;
				ptr += 6;
			}
		}
	}
	GLfloat outline[72] =
	{
		-0.5f, -0.5f, -0.5f, -0.5f, -0.5f, 0.5f,
		-0.5f, 0.5f, -0.5f, -0.5f, 0.5f, 0.5f,
		0.5f, -0.5f, -0.5f, 0.5f, -0.5f, 0.5f,
		0.5f, 0.5f, -0.5f, 0.5f, 0.5f, 0.5f,
		
		-0.5f, -0.5f, -0.5f, -0.5f, 0.5f, -0.5f,
		-0.5f, -0.5f, 0.5f, -0.5f, 0.5f, 0.5f,
		0.5f, -0.5f, -0.5f, 0.5f, 0.5f, -0.5f,
		0.5f, -0.5f, 0.5f, 0.5f, 0.5f, 0.5f,
		
		-0.5f, -0.5f, -0.5f, 0.5f, -0.5f, -0.5f,
		-0.5f, 0.5f, -0.5f, 0.5f, 0.5f, -0.5f,
		-0.5f, -0.5f, 0.5f, 0.5f, -0.5f, 0.5f,
		-0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f,
	};
	
	g::transformtoraster = mat4(
		g::X, 0.f, 0.f, 0.f,
		0.f, g::Y, 0.f, 0.f,
		0.f, 0.f, g::Z, 0.f,
		0.5f*(g::X-1), 0.5f*(g::Y-1), 0.5f*(g::Z-1), 1.f);
	
	/*
	 * cube
	 */
	glGenBuffers(4, g::buffers);
	
	glBindBuffer(GL_ARRAY_BUFFER, g::buffers[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, g::buffers[1]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
	
	glBindBuffer(GL_ARRAY_BUFFER, g::buffers[2]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(raster), raster, GL_STATIC_DRAW);
	
	glBindBuffer(GL_ARRAY_BUFFER, g::buffers[3]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(outline), outline, GL_STATIC_DRAW);
	
	/*
	 * program, shaders
	 */
	GLint status;
	
	g::program = glCreateProgram();
	
	const char *shader_src[2] =
		{
			"attribute vec3 coord3d;"
			"uniform mat4 viewprojection;"
			"uniform mat4 modeltoworld;"
			"void main(void)"
			"{"
				"gl_Position = viewprojection * modeltoworld * vec4(coord3d, 1.0);"
			"}",
			
			"uniform vec3 color;"
			"void main(void)"
			"{"
				"gl_FragColor = vec4(color, 1.0);"
			"}",
		};
	
	const GLenum type[2] = {GL_VERTEX_SHADER, GL_FRAGMENT_SHADER};
	
	for(int i=0; i<2; i++)
	{
		GLuint shader = glCreateShader(type[i]);
		if(!shader)
		{
			ThrowError("failed to create shader");
		}

		glShaderSource(shader, 1, &shader_src[i], 0);
		glCompileShader(shader);
		glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
		if(!status)
		{
			ThrowError("failed to compile shader ");
		}
				
		glAttachShader(g::program, shader);
	}
	
		
	glLinkProgram(g::program);
	glGetProgramiv(g::program, GL_LINK_STATUS, &status);
	if(!status)
	{
		ThrowError("failed to link program");
	}

	g::attribute_coord3d = glGetAttribLocation(g::program, "coord3d");
	if(g::attribute_coord3d == -1)
	{
		ThrowError("Could not bind attribute coord2d");
	}
	
	g::uniform_color = glGetUniformLocation(g::program,  "color");
	if(g::uniform_color == -1)
	{
		ThrowError("Could not bind attribute color");
	}
	
	g::uniform_viewprojection = glGetUniformLocation(g::program,  "viewprojection");
	if(g::uniform_viewprojection == -1)
	{
		ThrowError("Could not bind attribute viewprojection");
	}
	
	g::uniform_modeltoworld = glGetUniformLocation(g::program,  "modeltoworld");
	if(g::uniform_modeltoworld == -1)
	{
		ThrowError("Could not bind attribute modeltoworld");
	}
	
	/*
	 * box, blocks
	 */
	
	for(int i=0; i<g::numblocks; i++)
	{
		g::blocks[i].Build(g::filenames[i]);
	}
	
	g::box = new Box(g::X, g::Y, g::Z, g::blocks, g::numblocks);
	Box::message msg;
	msg.id_newblock = 0;
	g::box->Init(msg);
}

void destroy()
{
	delete g::box;
	
	glDeleteBuffers(4, g::buffers);
	glDeleteProgram(g::program);
}

void send(Box::message *msg)
{
	Message cmsg;
	cmsg.SetData((char*)msg, sizeof(Box::message));
	g::client->Write(cmsg);
}

void callbackfstate_tetris(Client::code c)
{
	
}

void callbackfmessage_tetris(const char *msg)
{
	Box::message bmsg;
	memcpy(&bmsg, msg, sizeof(Box::message));
	
	g::pauze = bmsg.pauze;
	g::goon = bmsg.go_on;
	g::game_over = bmsg.game_over;
	if(bmsg.dir != Box::none)
	{
		g::box->Arrow(!g::i_am_x, bmsg.dir);
	}
	if(bmsg.is_tick)
	{
		boost::thread t(boost::bind(&Box::Down, g::box, bmsg));
	}
	
	if(g::game_over)
	{
		Echo("Game over!");
	}
}

int main(int argc, char **argv)
{
	if(argc != 1 && argc != 3)
	{
		try
		{
			ThrowError("Usage : \ntetris\ntetris <host> <port>");
		}
		catch(const char*e)
		{
			return 1;
		}
	}
	
	if(!glfwInit())
	{
		ThrowError("Failed to initialize GLFW");
	}

	glfwDefaultWindowHints();
	glfwWindowHint(GLFW_RESIZABLE, false);
	GLFWwindow * const window = glfwCreateWindow(
		g::width,
		g::height,
		"Arne tetris 3d",
		nullptr,
		nullptr);
	if(!window)
	{
		ThrowError("Failed to open GLFW window");
	}
	glfwMakeContextCurrent(window);

	// Enable vertical sync (on cards that support it)
	glfwSwapInterval(true);

	GLenum err = glewInit();
	if(err != GLEW_OK)
	{
		fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
		ThrowError("failed to initiate glew");
	}

	if(!GLEW_VERSION_2_0)
	{
		ThrowError("your graphic card does not support OpenGL 2.0");
	}
	
	glEnable(GL_BLEND);
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	
	glLineWidth(3);
	
	glEnable(GL_POLYGON_SMOOTH);
	glEnable(GL_LINE_SMOOTH);
	
	glPolygonOffset(1, 1);
	
	glfwSetKeyCallback(window, keycall);
	glfwSetCursorPosCallback(window, mousemoved);
	
	init();
	
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	if(argc == 3)
	{
		g::client = new Client(argv[1], argv[2], &callbackfstate_tetris, &callbackfmessage_tetris);
		g::client->WaitForConnection();
		g::i_am_x = g::client->GetID() == 0;
		g::angle = g::i_am_x ? 0.f : 90.f;
		
		if(g::i_am_x)
		{
			boost::thread t(boost::bind(&Tick));
		}
	}
	else
	{
		boost::thread t(boost::bind(&Tick));
	}

	do
	{
		glfwPollEvents();
		view();
		onDisplay();
		glfwSwapBuffers(window);
	}while(g::goon && !glfwWindowShouldClose(window));
	// Check if the ESC key was pressed or the window was closed
	
	if(g::client)
	{
		Box::message msg;
		msg.go_on = g::goon;
		msg.game_over = g::game_over;
		send(&msg);
		g::client->Close();
		g::client->Join();
			
		delete g::client;
	}
	
	destroy();
	
	glfwTerminate();

	return 0;
}
