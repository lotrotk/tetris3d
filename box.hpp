#ifndef BOX_H
#define BOX_H

#include "block.hpp"
#include <deque>
#include <list>
#include <boost/interprocess/sync/interprocess_mutex.hpp>

class Box
{
public:
	typedef const Block *Cell;
	
	enum direction{up, left, right, down, none};
	
	struct message
	{
		message();
		
		int id_newblock;
		direction dir;
		bool is_move;
		bool pauze;
		bool is_tick;
		bool go_on;
		bool game_over;
	};
		
public:
	Box(int X, int Y, int Z, const Block *blocks, int blocks_count);
	~Box();
	
	void Init(message&);
	/**
	 * returns end game
	 */
	bool Down(message&);
	/**
	 * 
	 */
	Cell ***GetCells() const;
	/**
	 *
	 */
	void Arrow(bool x, direction arrow);
	

private:
	/**
	 * returns finished
	 */
	bool NewCycle(message&);
	/**
	 * overwrite cell only if empty
	 * returns wasempty
	 */
	bool Insert(Cell *cell, const Block *inserted);
	/**
	 * returns finished
	 */
	bool RePosition();
	/*
	 *
	 */
	inline Cell *GetCell(int x, int y, int z);
	
private:
	/**
	 * X,Y,Z
	 */
	int m_dims[3];
	/**
	 * by Y,X,Z
	 */
	Cell ***m_cells;
	
	const Block *m_current;
	glm::vec3 m_currentpos;
	glm::mat4 m_transform;
	
	std::deque<Cell*> m_currentcells;
	
	const Block *m_blocks;
	const int m_blocks_count;
	
	std::list<int> m_toberemoved;
	
	/**
	 * exclude Down and Arrow
	 */
	boost::interprocess::interprocess_mutex m_mutex;
};

#endif
