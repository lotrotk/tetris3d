#include "client.hpp"
#include <iostream>
#include <boost/bind.hpp>

using namespace std;
using boost::asio::ip::tcp;

void Client::callbackfs0(code)
{
	/*empty*/
}
void Client::callbackfm0(const char *m)
{
	cout << m << endl;
}

Client::Client(char *host, char *port, callbackfstate *cbfs, callbackfmessage *cbfm):
	m_port(atoi(port)),
	m_ioservice(),
	m_socket(m_ioservice),
	m_thread(),
	m_callbackfstate(cbfs),
	m_callbackfmessage(cbfm),
	m_id(-1)
{	
	tcp::resolver resolver(m_ioservice);
	tcp::resolver::query query(host, port);
		
	boost::asio::async_connect(m_socket,
		resolver.resolve(query),
		boost::bind(&Client::handle_connected, this, boost::asio::placeholders::error));

	m_thread = boost::thread(boost::bind(&boost::asio::io_service::run, &m_ioservice));
}

Client::~Client()
{
	do_close();
	m_ioservice.stop();
}

void Client::Write(const Message &msg)
{
	/*
	 * post : the handler will only be called in a thread in which the run(), run_one(), poll() or poll_one() member functions is currently being invoked
	 */
	m_ioservice.post(boost::bind(&Client::do_write, this, msg));
}

void Client::Close()
{
	m_ioservice.post(boost::bind(&Client::do_close, this));
	m_thread.join();
}

void Client::handle_connected(const boost::system::error_code& error)
{
	if(!error)
	{
		cout << "<connected>" << endl;
		m_id = 1;
		m_cond_connected.notify_all();
		m_callbackfstate(connection_made);
		/*
		 * read header
		 */
		
		do_read();
	}
	else
	{
		cerr << "<failed to connect>" << endl;
		cerr << "<waiting for connections>" << endl;
		do_close();
		m_callbackfstate(connection_waiting);
		/*
		 * wait for accept
		 */
		
		tcp::endpoint endpoint(tcp::v4(), m_port);
		tcp::acceptor acceptor(m_ioservice, endpoint);
		acceptor.accept(m_socket);
		cout << "<waiting stopped>" << endl << "<connected>" << endl;
		m_id = 0;
		m_cond_connected.notify_all();
		m_callbackfstate(connection_made);
		do_read();
	}
}

void Client::do_read()
{
	boost::asio::async_read(m_socket,
		boost::asio::buffer(m_readmsg.GetHeader(), Message::header_length),//Create a new buffer that represents the given memory range.
		boost::bind(&Client::handle_header_read, this, boost::asio::placeholders::error));
}

void Client::handle_header_read(const boost::system::error_code& error)
{
	if(!error)
	{
		/*
		 * read body
		 */
		boost::asio::async_read(m_socket,
			boost::asio::buffer(m_readmsg.GetBody(), m_readmsg.GetBodySize()),
			boost::bind(&Client::handle_body_read, this, boost::asio::placeholders::error));
	}
}

void Client::handle_body_read(const boost::system::error_code& error)
{
	if(error)
	{
		cerr << "<failed to read body>" << endl;
		do_close();
	}
	else
	{
		
		/*
		 * callbock
		 * read again
		 */
		m_callbackfmessage(m_readmsg.GetBody());
		do_read();
	}
}

void Client::handle_written(const boost::system::error_code& error)
{
	if(error)
	{
		cerr << "failed to write message" << endl;
		do_close();
	}
	else
	{
		/*
		 * remove written message, check if other messages remain
		 */
		 
		m_writemsgs.pop_front();
		
		if(!m_writemsgs.empty())
		{
			boost::asio::async_write(m_socket,
				boost::asio::buffer(m_writemsgs.front().GetHeader(), m_writemsgs.front().GetSize()),
				boost::bind(&Client::handle_written, this, boost::asio::placeholders::error));
		}
	}
}

void Client::do_write(Message msg)
{
	/*
	 * add the message to the queue
	 * if queue not being written (is empty) start writing
	 */
	
	bool write_in_progress = !m_writemsgs.empty();
	m_writemsgs.push_back(msg);
	if(!write_in_progress)
	{
		boost::asio::async_write(m_socket,
			boost::asio::buffer(m_writemsgs.front().GetHeader(), m_writemsgs.front().GetSize()),
			boost::bind(&Client::handle_written, this, boost::asio::placeholders::error));
	}
}

void Client::do_close()
{
	m_socket.close();
}

void Client::Join()
{
	m_thread.join();
}

int Client::GetID()
{
	return m_id;
}

void Client::WaitForConnection()
{
	boost::unique_lock<boost::mutex> lock(m_mutex_connection);
   m_cond_connected.wait(lock);
}
