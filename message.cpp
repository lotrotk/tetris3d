#include "message.hpp"

#include <cstring>
	
void Message::SetText(const char *body)
{
	i32 l = strlen(body);
	if(l > Message::max_body_length)
	{
		l = Message::max_body_length;
	}
	++l;
	
	*(i32*)m_data = l;
	
	memcpy(m_data+header_length, body, l);
}

void Message::SetData(void *data, int size)
{
	if(size > Message::max_body_length)
	{
		size = Message::max_body_length;
	}
	
	*(i32*)m_data = size;
	
	memcpy(m_data+header_length, data, size);
}

char *Message::GetHeader()
{
	return m_data;
}

char *Message::GetBody()
{
	return m_data + header_length;
}

int Message::GetBodySize()
{
	return *(i32*)m_data;
}

int Message::GetSize()
{
	return header_length+GetBodySize();
}
