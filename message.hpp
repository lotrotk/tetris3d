#ifndef MESSAGE_H
#define MESSAGE_H

#include <stdint.h>
typedef int32_t i32;

class Message
{
public:
	void SetText(const char*);
	void SetData(void*, int size);
	char *GetHeader(), *GetBody();
	int GetSize(), GetBodySize();

public:
	const static int header_length=4;
	const static int max_body_length=128;
private:
	char m_data[header_length+max_body_length+1];
};

#endif
